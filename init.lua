local periphery = require('periphery')
local I2C = periphery.I2C

local address



--[[
some variables and ideas for the code are taken from the example for the same 
display and the pi pico made by raspberry pi faundation
--]]

local LCD_CLEARDISPLAY = 0x01;
local LCD_RETURNHOME = 0x02;
local LCD_ENTRYMODESET = 0x04;
local LCD_DISPLAYCONTROL = 0x08;
local LCD_CURSORSHIFT = 0x10;
local LCD_FUNCTIONSET = 0x20;
local LCD_SETCGRAMADDR = 0x40;
local LCD_SETDDRAMADDR = 0x80;

-- flags for display entry mode
local LCD_ENTRYSHIFTINCREMENT = 0x01;
local LCD_ENTRYLEFT = 0x02;

-- flags for display and cursor control
local LCD_BLINKON = 0x01;
local LCD_CURSORON = 0x02;
local LCD_DISPLAYON = 0x04;

-- flags for display and cursor shift
local LCD_MOVERIGHT = 0x04;
local LCD_DISPLAYMOVE = 0x08;

-- flags for function set
local LCD_5x10DOTS = 0x04;
local LCD_2LINE = 0x08;
local LCD_8BITMODE = 0x10;

-- flag for backlight control
local LCD_BACKLIGHT = 0x08;

local LCD_ENABLE_BIT = 0x04;


-- Modes for lcd_send_byte
local LCD_CHARACTER=1
local LCD_COMMAND=0
  local messages = { --[[write bytes]]{ 0x00, 0x00 },  --[[reead message contents]]{ 0x00, 0x00, 0x00, flags = I2C.I2C_M_RD } }



  i2cLcd={}

function i2cLcd.init(device,i2cAddress)

lcd = I2C{device}

address=i2cAddress

end



function i2cLcd.goHome()
  
  i2c:transfer(address,LCD_RETURNHOME)

end


function i2cLcd.clear()
  i2c:transfer(address,LCD_CLEARDISPLAY)
end


function i2cLcd.goHome()

  local msgs = {
    --[[write bytes]]
    { 0x80, 0x84 },
    --[[reead message contents]]
    { 0x00, 0x00, 0x00, flags = I2C.I2C_M_RD }
  }
  i2c:transfer(address,msgs)

end


function i2cLcd.close()
  i2c:close()
end



return i2cLcd
